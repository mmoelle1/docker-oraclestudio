docker-oraclestudio
===================

[![pipeline status](https://gitlab.com/mmoelle1/docker-oraclestudio/badges/master/pipeline.svg)](https://gitlab.com/mmoelle1/docker-oraclestudio/commits/master)

Repository contains a customizable `Dockerfile` based on
`oraclelinux:7.8` and `oraclelinux:8.3` with **OracleStudio C/C++
compiler environment**. The generated docker images moreover contain
the tools `cmake`, `doxygen`, `git`, `ninja`, `svn`, `wget` and the
libraries `arrayfire`, `blas`, `boost`, and `lapack`. The finally they
contain the Intel OpenCL driver for Intel CPUs.

Available docker images:

**Oracle Linux 7.8**
- [registry.gitlab.com/mmoelle1/docker-oraclestudio:12.06oraclelinux7.8](registry.gitlab.com/mmoelle1/docker-oraclestudio:12.06oraclelinux7.8)

**Oracle Linux 8.3**
- [registry.gitlab.com/mmoelle1/docker-oraclestudio:12.06oraclelinux8.3](registry.gitlab.com/mmoelle1/docker-oraclestudio:12.06oraclelinux8.3)
